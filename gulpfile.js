var gulp = require('gulp'),
	concatCss    = require('gulp-concat-css'),
	rename       = require('gulp-rename'),
	notify       = require('gulp-notify'),
	livereload   = require('gulp-livereload'),
	connect      = require('gulp-connect'),
	autoprefixer = require('gulp-autoprefixer'),
    less         = require('gulp-less'),
    fileinclude  = require('gulp-file-include'),
    plumber      = require('gulp-plumber'),
    prettify     = require('gulp-html-prettify'),
    csso         = require('gulp-csso'),
    csscomb      = require('gulp-csscomb'),
    dirSync      = require('gulp-dir-sync');

//server connect
gulp.task('connect', function() {
  connect.server({
    root: 'public',
    livereload: true
  });
});

//paths
var paths = {
  js: 'build/js/*',
  css: 'build/less/*.css',
  less: 'build/less/all.less',
  html: 'build/*.html',
  fileinclude: 'build/include/*.html',
  images: 'build/images/*',

  src: 'build/',
  dest: 'public/'
};

//js
gulp.task('js', function() {
    gulp.src(paths.js)
    .pipe(gulp.dest(paths.dest + 'js/'))
    .pipe(connect.reload());
});

//css
gulp.task('css', function() {
    gulp.src(paths.css)
    .pipe(gulp.dest(paths.dest + 'css'))
    .pipe(notify('Done!'))
    .pipe(connect.reload());
});

//less
gulp.task('less', function() {
    gulp.src(paths.less)
    .pipe(plumber())
    .pipe(less())
    .pipe(autoprefixer({
        browsers: ['last 10 versions']
    }))
    .pipe(csscomb())
    .pipe(csso())
    .pipe(rename('all.min.css'))
    .pipe(gulp.dest('public/css'))
    .pipe(notify('Done!'))
    .pipe(connect.reload());
});

//fileinclude
gulp.task('fileinclude', function() {
  gulp.src(paths.html)
    .pipe(plumber())
    .pipe(fileinclude({
      prefix: '@@',
      basepath: '@file'
    }))
    .pipe(prettify({indent_char: ' ', indent_size: 2}))
    .pipe(gulp.dest(paths.dest))
    .pipe(connect.reload());
});

// Basic usage
gulp.task('sync', function() {
  dirSync('build/', 'public/');
});

//watch
gulp.task('watch', function(){
    gulp.watch(['sync'])
    gulp.watch(paths.js, ['js'])
    gulp.watch(paths.css, ['css'])
    gulp.watch('build/less/*.less', ['less'])
    gulp.watch(paths.html, ['fileinclude'])
    gulp.watch(paths.fileinclude, ['fileinclude'])
})

//default
gulp.task('default', ['fileinclude', 'js', 'css', 'less', 'watch', 'sync', 'connect']);
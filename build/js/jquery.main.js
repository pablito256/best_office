jQuery(document).ready(function(){
    $('input, textarea').placeholder();
    initPopup();

    $('.js-opener').sdToggle({
        parentOpenClass:'active',
        openLink:'.opener',
        closeLink:'.opener',
        target:'.slide',
        firstOpen:false,
        closeOther:true,
        speed: 400
    });
    $('.gallery').gallery({
        duration: 500,
        circle: true,
        effect: true,
        touch: true,
        listOfSlides: 'div.gallery__photo > div.gallery__photo-item',
        switcher: 'div.gallery__switcher > div.gallery__switcher-item'
    });
    $(".product-info__color-box a").on('click', function() {
      $(this).parent().siblings(".product-info__color-item").removeClass("active").end().addClass("active");
      return false;
    });
    $('.header__search-input').focus(function(){
         $('.header__search').addClass('focus');
    });
    $('.header__search-input').blur(function(){
         $('.header__search').removeClass('focus');
    });
});

function initPopup(){
    $('.header__call').each(function(){
        var holder = $(this);
        var target = $('.header__call-toggle', holder);
        var drop = $('.header__call-popup', holder);
        
        target.click(function(){
            drop.toggleClass('active');
            return false;
        })
        
        $('.close').click(function(){drop.removeClass('active');})
        $('body').keydown(function(e){
            if (e.keyCode == 27) drop.removeClass('active');
        })
    })
}
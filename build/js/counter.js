function initCounter(){
	$('.product__counter').on('click','.product__counter-up',function(e){
		e.preventDefault();
		var input = $(this).parent().find('.product__counter-input');
		if(input.val() < 100){
			var newCount = Number(input.val()) + 1;
			input.val(newCount);
		}
		checkChange(input);
	});
	$('.product__counter .product__counter-down').on('click',function(e){
		e.preventDefault();
		var input = $(this).parent().find('.product__counter-input');
		if(input.val() > 1){
			var newCount = Number(input.val()) - 1;
			input.val(newCount);
		}
		checkChange(input);
	});
	$('.product__counter .product__counter-input').on('input',function(){
		if ($(this).val() == 0 ){
			this.value = 1;
		}else if($(this).val() > 100){
			$(this).val(100);
		}else if ($(this).val().match(/[^0-9]/g)) {
			$(this).val($(this).val().replace(/[^0-9]/g, ''));
		}else if ($(this).val().match(/^0\d+/g)) {
			$(this).val($(this).val().replace(/^0\d+/g, '1'));
		}
		checkChange($(this));
	});	
	$('.product__counter .product__counter-input').on('focus',function(){
		
		if($._data(this, 'events').keypress == undefined){
			$(this).keypress(function(e){
				if(e.keyCode == 38){
					$(this).parent().find('.product__counter-up').trigger('click');
				}else if (e.keyCode == 40) {
					$(this).parent().find('.product__counter-down').trigger('click');
				}
			})
		};
	});

	function checkChange(input){
		if(input.val() > 1){
			input.parent().find('.product__counter-down').addClass('active');
		}
		else if(input.val() == 1){
			input.parent().find('.product__counter-down').removeClass('active');
		}
	}
}
$(document).ready(function(){
	initCounter();
});